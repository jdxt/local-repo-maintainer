# local repo maintainer

Collection of scripts to maintain my built from source repos.

## Usage

This will update all git repos in the root directory, re-build the updated repos
and then attempt to clean them.

Ignoring the build commands in the Makefile, the useful commands are:

```shell
$ make pull
./bin/pull.sh
REPO1: up to date.
REPO2:
# git output
# ...

$ make update
./bin/update.sh
UPDATING: REPO2
# build output
# ...

$ make clean
./bin/clean.sh
REPO1:
    found Makefile
    found Cargo.toml
REPO2:
    found Makefile
# ...
```
