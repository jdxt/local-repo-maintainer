all: help
help:
	# list of commands and corresponding dependencies
	echo && awk '/^(\w|-)+:/ { printf "%-25s%s\n",$$1,$$2 }' Makefile

pull: bin/pull.sh
	./bin/pull.sh

update: bin/update.sh
	./bin/update.sh

clean: bin/clean.sh
	./bin/clean.sh

#########
# FORKS #
#########
fork-i3: i3-gaps/
	(cd i3-gaps; \
		autoreconf --force --install)
	rm -rf i3-gaps/build/
	mkdir i3-gaps/build
	(cd i3-gaps/build/; \
		../configure --prefix=/usr --sysconfdir=/etc --disable-sanitizers; \
		make -j8; \
		sudo make install)
	echo -n "Restarting i3-gaps..."
	i3 restart; echo "done"

fork-i3lock: i3lock-color/
	(cd i3lock-color; \
		autoreconf --force --install)
	rm -rf i3lock-color/build
	mkdir i3lock-color/build; \
	(cd i3lock-color/build; \
		../configure --prefix=/usr --sysconfdir=/etc --disable-sanitizers; \
		make -j8)
	sudo cp i3lock-color/build/i3lock /usr/bin/i3lock

fork-picom: picom/
	sudo apt install libxext-dev libxcb1-dev libxcb-damage0-dev libxcb-xfixes0-dev libxcb-shape0-dev libxcb-render-util0-dev libxcb-render0-dev libxcb-randr0-dev libxcb-composite0-dev libxcb-image0-dev libxcb-present-dev libxcb-xinerama0-dev libpixman-1-dev libdbus-1-dev libconfig-dev libgl1-mesa-dev  libpcre2-dev  libevdev-dev uthash-dev libev-dev libx11-xcb-dev
	rm -rf picom/build || true
	(cd picom; \
		git submodule update --init --recursive; \
		meson --buildtype=release . build; \
		ninja -C build)
	echo "Restarting picom..."
	killall picom || true
	sudo cp picom/build/src/picom /usr/bin/picom
	picom -b

fork-st: st/
	(cd st; \
		sudo make clean install)

########
# RUST #
########
rust-xsv: xsv/
	(cd xsv; \
		make release; \
		cargo clean)

rust-hyperfine: hyperfine/
	(cd hyperfine; \
		cargo build --release)
	sudo cp hyperfine/target/release/hyperfine /usr/bin/hyperfine
	(cd hyperfine; \
		cargo clean)

repo-rust-analyzer: rust-analyzer/
	(cd rust-analyzer; \
		cargo xtask install --server; \
		cargo clean)

rust-alacritty: alacritty/
	cargo deb --install -p alacritty --manifest-path alacritty/Cargo.toml
	gzip -c alacritty/extra/alacritty.man | sudo tee /usr/local/share/man/man1/alacritty.1.gz > /dev/null
	sudo tic -xe alacritty,alacritty-direct,alacritty-256color alacritty/extra/alacritty.info

#########
# REPOS #
#########
repo-semver: semver-tool/
	(cd semver-tool; \
		sudo make install)

repo-farbfeld: farbfeld/
	(cd farbfeld; \
		make clean; \
		sudo make install)

repo-sent: sent/
	(cd sent; \
		make clean; \
		sudo make install)

repo-ckb: ckb-next/
	(cd ckb-next; \
		./quickinstall)

repo-mtpfs: simple-mtpfs/
	(cd simple-mtpfs; \
		make clean; \
		./configure; \
		make; \
		sudo make install)

repo-astroid: astroid/
	(cd astroid; \
		cmake -H. -Bbuild -GNinja; \
		sudo cmake --build build --target install)

repo-polybar: polybar/
	rm -rf polybar/build
	mkdir polybar/build
	(cd polybar/build; \
		cmake ..; \
		make -j8; \
		sudo make install)

repo-betterlockscreen: betterlockscreen/
	sudo cp betterlockscreen/betterlockscreen /usr/bin/
	sudo cp betterlockscreen/system/betterlockscreen@.service /etc/systemd/system/betterlockscreen@jay.service
	sudo systemctl enable betterlockscreen@jay

repo-rofi: rofi/
	(cd rofi; \
		meson setup build; \
		sudo ninja -C build install)

repo-webkit2-greeter: lightdm-webkit2-greeter/
	(cd lightdm-webkit2-greeter;\
		./autogen --prefix=/usr;\
		make;\
		sudo make install)
