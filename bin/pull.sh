#!/usr/bin/env bash
#
# pull.sh - run `git pull` in all directories and add updated repos to an
# "updates.txt" file

# Run `git pull` for each dir except bin/.
for dir in */; do
    if [ "$dir" = "bin/" ] || ! cd "$dir"; then
        continue
    fi

    echo -n "${dir^^}: "
    if [ "$dir" = "pfetch/" ]; then
        git checkout master
    fi

    if [ "$(git pull)" != "Already up to date." ]; then
        echo "$dir" >>../updates.txt
    else
        echo "up to date."
    fi

    if [ "$dir" = "pfetch/" ]; then
        git checkout parrot
    fi

    cd ..
done
[ -r updates.txt ] && cat updates.txt
exit 0
