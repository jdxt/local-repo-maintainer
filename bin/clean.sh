#!/usr/bin/env bash
#
# clean.sh - calls 'clean' functions for various types of repos.
#
# It is assumed that this is called from the parent directory of multiple repos.

# This associative array maps a file name to the corresponding command to run.
declare -A commands; command(){ commands[$1]=$2; }
command "Makefile"      "make clean; make distclean"
command "Cargo.toml"    "cargo clean"
command "__pycache__"   "rm -rf __pycache__"
command "main.go"       "go clean"

# `cd` into directories and run clean commands if corresponding file(s) exists.
# Ignore exit codes.
for dir in */; do
    # directories to ignore
    case $dir in
        "bin/")
            echo "$dir: SKIPPED"
            continue;;
    esac

    if ! cd "$dir"; then
        echo "Can't cd into $dir"
        continue
    fi

    for file in "${!commands[@]}"; do
        if [ -r "$file" ]; then
            echo "$dir:"
            printf "\tfound %s\n" "$file"
            eval "${commands[$file]}" >/dev/null 2>&1
        fi
    done

    cd ..
done

