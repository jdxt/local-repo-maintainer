#!/usr/bin/env bash
#
# update.sh - Uses "updates.txt" generated by "pull.sh" and a Makefile to run
# the corresponding make commands.

# Bail if no readable "updates.txt" found.
if [ ! -r updates.txt ]; then
    echo "Nothing to update. Try \`make pull\` first."
    exit 1
fi

# Load the "cmd: dep" lines from the Makefile into an array.
# Assumes commands are in the format "word(-word)+"
declare -a lines
mapfile -t lines < <(grep -E '^\w+(-\w+)+' Makefile | sed 's/://; s/\///')

# Turn lines array into a `dir=cmd` map (associative array).
declare -A map
for line in "${lines[@]}"; do
    eval "$(echo "$line" | awk '{printf "map[\"%s/\"]=%s",$2,$1}')"
done

# Read each line from "updates.txt" and run corresponding make command.
# If no command specified in Makefile, echo warning.
# If the command exits with non-zero, add to "rejects.txt"
while read -r dir; do
    cmd=${map[$dir]}
    [ -z "$cmd" ] && echo "WARN: No Makefile command for $dir" && continue

    echo "UPDATING: $dir"
    if ! make "$cmd"; then
        echo "unsuccessful updating $dir"
        echo "$dir" >>rejects.txt
    fi
done <updates.txt

echo
# Finally, remove "updates.txt" and show "rejects.txt"
[ -w updates.txt ] && rm updates.txt
[ -r rejects.txt ] && echo "REJECTS:" && cat rejects.txt && $EDITOR rejects.txt
exit 0
